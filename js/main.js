$(function () {
  // Code here
  let btnLogin = document.querySelector("#btnLogin"),
    btnRegister = document.querySelector("#btnRegister"),
    btn = document.querySelector("#boton"),
    login = document.querySelector("#login"),
    register = document.querySelector("#register"),
    form = document.querySelector(".form-box");

  btnRegister.addEventListener("click", function () {
    login.style.left = "-400px";
    register.style.left = "50px";
    btn.style.left = "110px";
    form.style.height = "700px";
  });

  btnLogin.addEventListener("click", function () {
    login.style.left = "50px";
    register.style.left = "450px";
    btn.style.left = "0";
    form.style.height = "300px";
  });
  $(".hero").mousemove(function (e) {
    var moveX = (e.pageX * -1) / 30;
    var moveY = (e.pageY * -3) / 30;
    $(this).css("background-position", moveX + "px " + moveY + "px");
  });
});
